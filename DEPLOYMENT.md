# Deployment

Journal is deployed and hosted by Google Firebase, drastically simplifying the deployment and configuration process. Where possible, configuration has been abstracted to configuration files within the project ([`firebase.json`](firebase.json)).

## Firebase

Configuring a new Firebase project for Journal is a relatively straightforward task ([Firebase Web Setup](https://firebase.google.com/docs/web/setup)).

### Project

- Create Firebase project (_customize project ID_)
- Create **web** app in project (_copy project config variables_)
- Enable project features
  - Authentication - Add Google provider
  - Firestore - Validate access rules and add **indices**
  - Hosting - Add custom domain

> **NOTE:** A separate Firebase project is required for each deployment environment!

### Domain

Custom domains can be enable via Firebase Hosting, and require configuring 2 `A` DNS records to point the subdomain to the Firebase servers.

> **NOTE:** Customizing the auth redirect URL is slightly more involved and currently undocumented.

## Environment Variables

Environment variables are used to extract environment-dependent or secret configuration out of the repository. These variables must be set in development ([sample `.env`](.env.example)), manual deploys, **and** in the GitLab pipelines.

GitLab CI configuration requires assigning variables to the designated release environment (`staging`, etc), enabling using the same variable for multiple environments. Failure to set these variables will result in a broken deployment!

> **NOTE:** Remember to set GitLab CI variables for each target environment!

| Variable | Description |
|----------|-------------|
| `FIREBASE_DEPLOY_KEY` | Firebase CLI deploy token (granted via `firebase login:ci`)
| `VUE_APP_FIREBASE_API_KEY` | Firebase API key
| `VUE_APP_FIREBASE_APP_ID` | Firebase app ID (_within project_)
| `VUE_APP_FIREBASE_AUTH_DOMAIN` | Firebase auth redirect domain
| `VUE_APP_FIREBASE_MESSAGING_ID` | _Firebase messaging ID (unused)_
| `VUE_APP_FIREBASE_PROJECT_ID` | Firebase project ID
| `VUE_APP_FIREBASE_STORAGE_BUCKET` | _Firebase storage bucket (unused)_

> **NOTE:** _`VUE_APP_FIREBASE_API_KEY` must be set in `testing` environment; tests will fail otherwise due to file dependencies on Firebase config!_

## Deploying

Deploying the app can be done through the GitLab pipelines (_recommended_) or manually with the `firebase-tools` CLI. Deployments use the [`firebase.json`](firebase.json) and [`.firebaserc`](.firebaserc) files for configuration and project aliases; the `default` alias has been removed since different projects are used for different environments.

> **NOTE:** Ensure appropriate environment variables have been set for target environment!

### Pipeline

GitLab pipelines are already configured to build the project on merges into `master` and other deployment environments (ie. `production`). Once the pipelines have passed and the build is completed, the deploy can be trigged manually from the pipeline.

### Manual

Manual deployments are done through the [Firebase CLI](https://firebase.google.com/docs/cli).

```sh
# Install Firebase CLI
npm install -g firebase-tools

# Login to Firebase
firebase login

# Deploy with Firebase
firebase deploy -P <project_alias> -m "<deploy_message>"
```

> Token access can be revoked with `firebase logout --token <token>`.
