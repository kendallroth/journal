#!/bin/bash

###########################################################
# Validate that all required environment variables are set

# Environment variables that must be set
variables=(
  "FIREBASE_DEPLOY_KEY"
  "VUE_APP_FIREBASE_API_KEY"
  "VUE_APP_FIREBASE_APP_ID"
  "VUE_APP_FIREBASE_AUTH_DOMAIN"
  "VUE_APP_FIREBASE_MESSAGING_ID"
  "VUE_APP_FIREBASE_PROJECT_ID"
  "VUE_APP_FIREBASE_STORAGE_BUCKET"
)
unset_variables=()

# Track unset variables (for error message)
for variable in ${variables[@]}
do
  if [[ ! ${!variable} ]]; then
    unset_variables+=($variable)
  fi
done

# Fail if any variables are unset
if [[ ${#unset_variables[@]} > 0 ]]; then
  echo "ERROR: Unset environment variables!"

  for variable in ${unset_variables[@]}
  do
    echo " - $variable"
  done

  exit 1
else
  echo "SUCCESS: All required environment variables set"
fi
