module.exports = {
  preset: "@vue/cli-plugin-unit-jest/presets/typescript-and-babel",
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "^@components(.*)$": "<rootDir>/src/components/$1",
    "^@services(.*)$": "<rootDir>/src/services$1",
    "^@store(.*)$": "<rootDir>/src/store$1",
    "^@typings(.*)$": "<rootDir>/src/types$1",
    "^@utilities(.*)$": "<rootDir>/src/utilities$1",
    "^@views(.*)$": "<rootDir>/src/views$1",
  },
  // setupFilesAfterEnv: ["<rootDir>src/setupTests.ts"],
  testMatch: ["**/*.test.ts"],
  // NOTE: Need to transpile/transform some node_module packages during tests
  transformIgnorePatterns: ["/node_modules/(?!(vuetify)/)"],
};
