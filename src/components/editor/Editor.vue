<template>
  <div class="editor">
    <v-textarea
      v-model="entryForm.fields.text"
      class="editor__input"
      hideDetails
      noResize
      outlined
      rows="8"
    />
    <v-card-actions class="editor__actions">
      <div class="editor__actions__left">
        <!-- Entry mood selector -->
        <v-menu v-model="areEmoticonsShown" offset-y>
          <template v-slot:activator="{ attrs, on }">
            <v-btn
              :disabled="entryForm.flags.loading"
              depressed
              fab
              small
              v-bind="attrs"
              v-on="on"
            >
              <v-icon v-if="selectedEntryMood" :color="selectedEntryMood.color">
                {{ selectedEntryMood.icon }}
              </v-icon>
              <v-icon v-else color="primary">{{ icons.mdiEmoticon }}</v-icon>
            </v-btn>
          </template>
          <EditorIconSelector
            v-model="entryForm.fields.mood"
            :icons="moodIcons"
          />
        </v-menu>

        <v-btn
          :disabled="entryForm.flags.loading"
          depressed
          fab
          small
          @click="toggleHighlight"
        >
          <v-icon :color="entryForm.fields.highlight ? 'amber' : 'primary'">
            {{
              entryForm.fields.highlight ? icons.mdiStar : icons.mdiStarOutline
            }}
          </v-icon>
        </v-btn>

        <!-- Entry date picker -->
        <v-dialog v-model="isDatePickerShown" width="300px">
          <template v-slot:activator="{ attrs, on }">
            <v-btn
              :disabled="entryForm.flags.loading"
              depressed
              fab
              small
              v-bind="attrs"
              v-on="on"
            >
              <v-icon color="primary">{{ icons.mdiCalendar }}</v-icon>
            </v-btn>
          </template>
          <v-date-picker
            v-model="entryForm.fields.date"
            :max="today"
            showAdjacentMonths
            @change="isDatePickerShown = false"
          />
        </v-dialog>
        <div class="editor__actions__date">
          {{ entryDateFormatted }}
        </div>
      </div>

      <v-spacer />

      <v-btn
        :disabled="!entryForm.flags.changed || entryForm.flags.loading"
        icon
        @click="isResetDialogShown = true"
      >
        <v-icon>{{ icons.mdiClose }}</v-icon>
      </v-btn>
      <v-btn
        :disabled="!canSubmit"
        :loading="entryForm.flags.submitting"
        color="primary"
        depressed
        fab
        small
        @click="submit"
      >
        <v-icon>{{ icons.mdiCheck }}</v-icon>
      </v-btn>
    </v-card-actions>
    <ConfirmDialog
      v-model="isResetDialogShown"
      title="Reset Entry"
      @confirm="resetEntry"
    >
      Are you sure you want to reset this entry?
    </ConfirmDialog>
  </div>
</template>

<script lang="ts">
import { Component, Prop, Watch, Vue } from "vue-property-decorator";
import dayjs from "dayjs";
import { createForm } from "@kendallroth/vue-simple-forms";
import {
  mdiCalendar,
  mdiCheck,
  mdiClose,
  mdiEmoticonOutline,
  mdiStar,
  mdiStarOutline,
} from "@mdi/js";

// Components
import EditorIconSelector from "./EditorIconSelector.vue";

// Utilities
import { DATE_FORMAT, DATE_FORMAT_READABLE } from "@/constants";
import { moodIcons, moodIconList } from "./helpers";

// Types
import { EditorIconSelectorIcon } from "@typings/editor";
import { Entry, EntryForm } from "@typings/entry";

const today = dayjs().format(DATE_FORMAT);

const editorFormFields: EntryForm = {
  date: today,
  highlight: false,
  mood: null,
  text: "",
};

@Component({
  components: {
    EditorIconSelector,
  },
})
export default class Editor extends Vue {
  /** Journal entry */
  @Prop({ default: null })
  entry!: Entry | null;
  /** Save handler (since child handles form) */
  @Prop({ required: true })
  onSave!: (values: EntryForm) => void;

  /** Update form when parent jornal entry changes (since dialogs do not mount/unmount) */
  @Watch("entry", { deep: true, immediate: true })
  onEntryChanged(newEntry: Entry | null): void {
    if (!newEntry) {
      this.entryForm.setInitial({ ...editorFormFields });
      return;
    }

    this.entryForm.setInitial({
      date: dayjs(newEntry.date).format(DATE_FORMAT),
      highlight: newEntry.highlight,
      mood: newEntry.mood,
      text: newEntry.text,
    });
  }

  areEmoticonsShown = false;
  isDatePickerShown = false;
  isResetDialogShown = false;

  entryForm = createForm({ ...editorFormFields });

  moodIcons = moodIconList;
  today = today;

  icons = {
    mdiCalendar,
    mdiCheck,
    mdiClose,
    mdiEmoticon: mdiEmoticonOutline,
    mdiStar,
    mdiStarOutline,
  };

  /** Whether the form can be submitted */
  get canSubmit(): boolean {
    const { text } = this.entryForm.getValues() as EntryForm;

    return Boolean(text?.trim());
  }

  /** Selected entry mood */
  get selectedEntryMood(): EditorIconSelectorIcon | null {
    const { mood } = this.entryForm.getValues() as EntryForm;
    if (!mood) return null;

    return moodIcons[mood] ?? null;
  }

  /** Formatted journal entry date */
  get entryDateFormatted(): string {
    const { date } = this.entryForm.getValues() as EntryForm;

    return dayjs(date).format(DATE_FORMAT_READABLE);
  }

  /**
   * Toggle whether the day was a highlight
   */
  toggleHighlight(): void {
    this.entryForm.setValues({
      highlight: !this.entryForm.fields.highlight,
    });
  }

  /**
   * Submit the editor entry
   */
  async submit(): Promise<void> {
    const { changed } = this.entryForm.flags;
    if (!changed) return;

    const values = this.entryForm.getValues() as EntryForm;
    if (!values.text) return;

    this.entryForm.setSubmitting(true);

    try {
      await this.onSave(values);
    } catch (e) {
      this.entryForm.setSubmitting(false);
      return;
    }

    this.entryForm.setSubmitting(false);
    this.entryForm.reset();
  }

  /**
   * Reset the editor
   */
  resetEntry(): void {
    this.entryForm.reset();
  }
}
</script>

<style lang="scss" scoped>
$editor-spacing: 12px;

.editor {
  width: 100%;
  padding: 0;
}

.editor__actions {
  margin-top: 16px;
  padding: 0;
}

.editor__actions__left {
  display: flex;
  align-items: center;

  > *:not(:first-child) {
    margin-left: 8px;
  }
}

.editor__actions__date {
  color: map-get($blue-grey, lighten-1);
}
</style>

<style lang="scss">
.editor__input textarea {
  line-height: 1.5rem !important;
}
</style>
