import { v4 as uuidv4 } from "uuid";

// Utilities
import UserModule from "./user";

// Types
import { User } from "@typings/user";

const user: User = {
  activatedAt: new Date(),
  displayName: "John Doe",
  entries: 3,
  email: "hello@example.com",
  phoneNumber: null,
  photoUrl: null,
  uid: uuidv4(),
};

describe("Store - User", () => {
  beforeEach(() => {
    UserModule.initialize();
  });

  it("Should start with default state", () => {
    expect(UserModule.authenticating).toBe(false);
    expect(UserModule.authenticated).toBe(false);
    expect(UserModule.user).toBe(null);
  });

  it("Should set authenticating status", () => {
    UserModule.setAuthenticating(true);

    expect(UserModule.authenticating).toBe(true);
  });

  it("Should set user", () => {
    UserModule.setUser({ ...user });

    expect(UserModule.authenticated).toBe(true);
    expect(UserModule.user).not.toBe(null);
    expect(UserModule.user).toStrictEqual(user);
  });

  it("Should remove user", () => {
    UserModule.setUser({ ...user });

    UserModule.removeUser();

    expect(UserModule.authenticated).toBe(false);
    expect(UserModule.user).toBe(null);
  });

  it("Should change user entry counter", () => {
    UserModule.setUser({ ...user });

    UserModule.addEntry();
    const countAfterAdding = UserModule.user?.entries;

    UserModule.deleteEntry();
    const countAfterRemoving = UserModule.user?.entries;

    expect(countAfterAdding).toBe(user.entries + 1);
    expect(countAfterRemoving).toBe(user.entries);
  });
});
