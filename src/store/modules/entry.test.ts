import { v4 as uuidv4 } from "uuid";

// Utilities
import EntryModule from "./entry";

// Types
import { Entry } from "@typings/entry";
import { Moods } from "@typings/enums";

const entry: Entry = {
  id: uuidv4(),
  createdAt: new Date(),
  date: new Date(),
  highlight: false,
  mood: Moods.CONTENT,
  text: "Sample entry text",
  updatedAt: null,
  userId: uuidv4(),
};

describe("Store - Entry", () => {
  beforeEach(() => {
    EntryModule.initialize();
  });

  it("Should start with default state", () => {
    expect(EntryModule.entries).toStrictEqual([]);
    expect(EntryModule.loading).toBe(false);
  });

  it("Should add entry", () => {
    EntryModule.addEntry({ ...entry });

    expect(EntryModule.entries.length).toBe(1);
    expect(EntryModule.entries[0]).toStrictEqual(entry);
  });

  it("Should remove entry", () => {
    EntryModule.addEntry({ ...entry });

    EntryModule.deleteEntry({ ...entry });

    expect(EntryModule.entries.length).toBe(0);
  });

  it("Should update entry", () => {
    EntryModule.addEntry({ ...entry });

    const newEntry = { ...entry, text: "Updated text" };
    EntryModule.updateEntry({ ...newEntry });

    expect(EntryModule.entries[0]).toStrictEqual(newEntry);
  });

  it("Should set entries", () => {
    EntryModule.setEntries([{ ...entry }]);

    expect(EntryModule.entries.length).toBe(1);
    expect(EntryModule.entries[0]).toStrictEqual(entry);
  });

  it("Should set entries loading", () => {
    EntryModule.setLoading();

    expect(EntryModule.loading).toBe(true);
  });
});
