import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

// NOTE: Dynamic modules cannot be imported when configuring store!
export default new Vuex.Store({});
