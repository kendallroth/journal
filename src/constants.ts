/** Default date format */
export const DATE_FORMAT = "YYYY-MM-DD";
/** Readable date format */
export const DATE_FORMAT_READABLE = "MMM DD, YYYY";
/** Media query visibility storage key */
export const MEDIA_QUERY_VISIBILITY_KEY = "isMediaQueryVisible";
