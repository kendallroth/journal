import Vue from "vue";
import VueRouter from "vue-router";

// Utilities
import guards from "./guards";
import routes from "./routes";

Vue.use(VueRouter);

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

// Hook up router navigation guards
guards(router);

export default router;
export * from "./utilities";
