/** User */
export interface User extends UserCredentials, UserProfile {}

/** Extra user profile (internal firebase data) */
export interface UserProfile {
  /** Account activation date */
  activatedAt: Date | null;
  /** Number of entries */
  entries: number;
}

/** User credentials */
export interface UserCredentials {
  /** Display name */
  displayName: string;
  /** Email address */
  email: string;
  /** Phone number */
  phoneNumber: string | null;
  /** Profile photo URL */
  photoUrl: string | null;
  /** Firebase user ID */
  uid: string;
}
