import { FormFields } from "@kendallroth/vue-simple-forms/lib/types";

// Types
import { Moods } from "./enums";

/** Journal entry */
export interface Entry {
  /** Journal entry creation timestamp (may differ from 'date') */
  createdAt: Date;
  /** Journal entry date */
  date: Date;
  /** Journal entry mood */
  mood: Moods | null;
  /** Whether the day was a highlight */
  highlight: boolean;
  /** Journal entry ID */
  id: string;
  /** Journal entry text */
  text: string;
  /** Journal entry update timestamp */
  updatedAt: Date | null;
  /** Journal entry user */
  userId: string;
}

/** Entry form fields */
export interface EntryForm extends FormFields {
  date: string;
  highlight: boolean;
  mood: Moods | null;
  text: string;
}
