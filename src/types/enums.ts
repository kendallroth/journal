/** Daily moods */
export enum Moods {
  UPSET = "UPSET",
  SAD = "SAD",
  CONTENT = "CONTENT",
  HAPPY = "HAPPY",
  EXCITED = "EXCITED",
}
