/** Editor icon selector icon */
export interface EditorIconSelectorIcon {
  /** Icon colour */
  color: string;
  /** Icon SVG path */
  icon: string;
  /** Icon name/value */
  value: string;
}
