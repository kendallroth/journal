import firebase from "firebase/app";

/** Firestore document data */
export type DocumentData = firebase.firestore.DocumentData;
/** Firestore user */
export type FirebaseUser = firebase.User;
/** Firestore timestamp */
export type FirestoreTimestamp = firebase.firestore.Timestamp;
/** Firestore data converter */
export type FirestoreDataConverter = firebase.firestore.FirestoreDataConverter;
/** Firestore query document snapshot */
export type QueryDocumentSnapshot = firebase.firestore.QueryDocumentSnapshot;
/** Firestore snapshot options */
export type SnapshotOptions = firebase.firestore.SnapshotOptions;
