module.exports = {
  root: true,
  env: {
    node: true,
    jest: true,
  },
  extends: [
    "plugin:vue/essential",
    "eslint:recommended",
    "@vue/typescript/recommended",
    "@vue/prettier",
    "@vue/prettier/@typescript-eslint",
  ],
  parserOptions: {
    ecmaVersion: 2020,
  },
  rules: {
    "no-console": "warn",
    "no-debugger": "warn",
    "prefer-const": "warn",
    "@typescript-eslint/ban-ts-ignore": "off",
    "@typescript-eslint/camelcase": "warn",
    "@typescript-eslint/explicit-function-return-type": "warn",
    "@typescript-eslint/interface-name-prefix": "off",
    "vue/no-unused-vars": "warn",
  },
  overrides: [
    {
      files: [
        "**/__tests__/*.{j,t}s?(x)",
        "**/tests/unit/**/*.spec.{j,t}s?(x)",
      ],
      env: {
        jest: true,
      },
    },
  ],
};
